﻿
// TriangGalerkinDlg.h: файл заголовка
//

#pragma once

#include <vector>

using namespace std;

struct Dots
{
	double x = 0.;
	double y = 0.;
	double z = 0.;
	double potenc = 0.;
	bool sv = false;
	bool dr = false;
	bool gran = false;
	bool north1 = false;
	bool south1 = false;
	bool north2 = false;
	bool south2 = false;
	bool del = false;
};

struct Triangle
{
	Dots A;
	Dots B;
	Dots C;
};

// Диалоговое окно CTriangGalerkinDlg
class CTriangGalerkinDlg : public CDialogEx
{
	// Создание
public:
	CTriangGalerkinDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TRIANGGALERKIN_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	//области рисования
	CWnd* PicWndGraph;
	CDC* PicDcGraph;
	CRect PicGraph;

	double MinX = -10, MaxX = 10, MinY = -10, MaxY = 10, Min2 = -10, Max2 = 10;

	double xpGraph = 0, ypGraph = 0,			//коэфициенты пересчета
		xminGraph = -1, xmaxGraph = 1,			//максисимальное и минимальное значение х 
		yminGraph = -0.5, ymaxGraph = 5;			//максисимальное и минимальное значение y
public:
	CButton potenc_draw;
	double sverchX_1;	 double sverchY_1;
	double sverchX_2;	 double sverchY_2;
	double sverchX_3;	 double sverchY_3;
	double sverchX_4;	 double sverchY_4;

	double oblX_1;	 double oblY_1;
	double oblX_2;	 double oblY_2;
	double oblX_3;	 double oblY_3;
	double oblX_4;	 double oblY_4;

	double width1;	 double width2;
	double height1;	 double height2;
	double angle1;	 double angle2;

	double centerX_1;	 double centerY_1;
	double centerX_2;	 double centerY_2;

	double step;
	double U_dyrki;
	double U_gran;

	int scale = 1;
	double defaultX0 = 0.0;
	double defaultY0 = 0.0;
	double minRand = -0.1;		double maxRand = 0.2;	double Rand = maxRand * 2;
	double amplituda = 0;

	vector<Triangle> recTriang;
	vector<Dots> PointsSverch;
	vector<Dots> randPoints;
	vector<Dots> square1;
	vector<Dots> squareHelp1;
	vector<Dots> square2;
	vector<Dots> squareHelp2;

	void DrawDC(CDC* WinDc, CRect WinPic, double AbsMax);
	void DrawTriang(vector<Dots> rand, vector<Triangle> treug, CDC* WinDc, CRect Winxmax);
	void DrawForce(vector<vector<Dots>> line, vector<Dots> rand, vector<Triangle> treug, CDC* WinDc, CRect Winxmax);
	void DrawEqui(vector<vector<Dots>> line, vector<Dots> rand, vector<Triangle> treug, CDC* WinDc, CRect Winxmax);
	double Described(Dots iDots, Dots jDots, Dots kDots, double& XC, double& YC);
	void TriangFunc(int pointN, vector<Dots>& point, vector<Triangle>& triangleABC);
	void Delete(vector<Triangle>& vecTriang);
	void Sverch(vector<Dots>& vec);
	void CalcDyrky();
	bool enters(int npol, vector<Dots> kvadr, double x, double y);
	bool UpDown(double x, double y, double x1, double y1, double x2, double y2);
	void randPointFunc(vector<Dots>& vec);
	void AnimationInformation();
	void vertexUp(Dots pt, Triangle triang, Triangle& triang_out);
	void DotsNeighbours(Dots i, Dots j, vector<Triangle> dln, vector<Triangle>& neighbours);
	void matrixAij(vector<Dots> vecVnutr, vector<double>& MatrAij);
	void vectorBj(vector<Dots> vecVnutr, vector<Dots> vecGran, vector<double>& vecj);
	void kazf(vector<double> a, vector<double>  b, vector<double>& x, int nn, int ny);
	void intersect(double a1, double a2, double b1, double b2, double c1, double c2, double& x, double& y);
	double GetS(double x1, double y1, double x2, double y2, double x3, double y3);
	void forceLines(vector<Dots> vecMagn, vector<Triangle> triangleABC, vector<vector<Dots>>& lineForce);
	void equipotencLines(vector<Triangle> triangleABC, vector<vector<Dots>>& lineEqui);

	afx_msg void OnBnClickedTriang();
	afx_msg void OnBnClickedDelete();
	afx_msg void OnBnClickedGalerkin();
	afx_msg void OnBnClickedForce();
	afx_msg void OnBnClickedCancel();
	afx_msg void OnBnClickedEquiline();
};
