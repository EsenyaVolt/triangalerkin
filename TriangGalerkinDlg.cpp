﻿
// TriangGalerkinDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "TriangGalerkin.h"
#include "TriangGalerkinDlg.h"
#include "afxdialogex.h"

#include <fstream>
#include <iomanip>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// Диалоговое окно CTriangGalerkinDlg

#define DOTSGRAPH(x,y) (xpGraph*((x)-xminGraph)),(ypGraph*((y)-ymaxGraph))

CTriangGalerkinDlg::CTriangGalerkinDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TRIANGGALERKIN_DIALOG, pParent)
	, sverchX_1(-50)
	, sverchX_2(-50)
	, sverchX_3(50)
	, sverchX_4(50)
	, sverchY_1(-50)
	, sverchY_2(50)
	, sverchY_3(50)
	, sverchY_4(-50)
	, oblX_1(-40)
	, oblX_2(-40)
	, oblX_3(40)
	, oblX_4(40)
	, oblY_1(-30)
	, oblY_2(30)
	, oblY_3(30)
	, oblY_4(-30)
	, width1(8)
	, width2(10)
	, height1(32)
	, height2(20)
	, centerX_1(-20)
	, centerX_2(20)
	, centerY_1(5)
	, centerY_2(-5)
	, angle1(-15)
	, angle2(31)
	, step(2)
	, U_dyrki(5)
	, U_gran(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTriangGalerkinDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, sverchX_1);
	DDX_Text(pDX, IDC_EDIT2, sverchX_2);
	DDX_Text(pDX, IDC_EDIT3, sverchX_3);
	DDX_Text(pDX, IDC_EDIT4, sverchX_4);
	DDX_Text(pDX, IDC_EDIT5, sverchY_1);
	DDX_Text(pDX, IDC_EDIT6, sverchY_2);
	DDX_Text(pDX, IDC_EDIT7, sverchY_3);
	DDX_Text(pDX, IDC_EDIT8, sverchY_4);
	DDX_Text(pDX, IDC_EDIT9, oblX_1);
	DDX_Text(pDX, IDC_EDIT10, oblX_2);
	DDX_Text(pDX, IDC_EDIT11, oblX_3);
	DDX_Text(pDX, IDC_EDIT12, oblX_4);
	DDX_Text(pDX, IDC_EDIT13, oblY_1);
	DDX_Text(pDX, IDC_EDIT14, oblY_2);
	DDX_Text(pDX, IDC_EDIT15, oblY_3);
	DDX_Text(pDX, IDC_EDIT16, oblY_4);
	DDX_Text(pDX, IDC_EDIT17, width1);
	DDX_Text(pDX, IDC_EDIT22, width2);
	DDX_Text(pDX, IDC_EDIT18, height1);
	DDX_Text(pDX, IDC_EDIT23, height2);
	DDX_Text(pDX, IDC_EDIT19, centerX_1);
	DDX_Text(pDX, IDC_EDIT24, centerX_2);
	DDX_Text(pDX, IDC_EDIT20, centerY_1);
	DDX_Text(pDX, IDC_EDIT25, centerY_2);
	DDX_Text(pDX, IDC_EDIT21, angle1);
	DDX_Text(pDX, IDC_EDIT26, angle2);
	DDX_Text(pDX, IDC_EDIT27, step);
	DDX_Text(pDX, IDC_EDIT29, U_dyrki);
	DDX_Text(pDX, IDC_EDIT30, U_gran);
	DDX_Control(pDX, IDC_POTENC, potenc_draw);
}

BEGIN_MESSAGE_MAP(CTriangGalerkinDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_TRIANG, &CTriangGalerkinDlg::OnBnClickedTriang)
	ON_BN_CLICKED(IDC_DELETE, &CTriangGalerkinDlg::OnBnClickedDelete)
	ON_BN_CLICKED(IDC_GALERKIN, &CTriangGalerkinDlg::OnBnClickedGalerkin)
	ON_BN_CLICKED(IDC_BUTTON1, &CTriangGalerkinDlg::OnBnClickedForce)
	ON_BN_CLICKED(IDCANCEL, &CTriangGalerkinDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_EQUILINE, &CTriangGalerkinDlg::OnBnClickedEquiline)

END_MESSAGE_MAP()

// Обработчики сообщений CTriangGalerkinDlg
BOOL CTriangGalerkinDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	//для  картинки
	PicWndGraph = GetDlgItem(IDC_GRAPH);
	PicDcGraph = PicWndGraph->GetDC();
	PicWndGraph->GetClientRect(&PicGraph);

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CTriangGalerkinDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
		DrawDC(PicDcGraph, PicGraph, 50);
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CTriangGalerkinDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CTriangGalerkinDlg::DrawDC(CDC* WinDc, CRect WinxmaxGraphc, double AbsMax)
{
	//область построения
	xminGraph = -AbsMax * 0.08;			//минимальное значение х
	xmaxGraph = AbsMax * 1.25;			//максимальное значение х
	yminGraph = -MaxX * 0.1;			//минимальное значение y
	ymaxGraph = MaxX * 1.15;		//максимальное значение y

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double widthX = WinxmaxGraphc.Width() * scale;
	double heightY = WinxmaxGraphc.Height() * scale;
	xpGraph = (widthX / (xmaxGraph - xminGraph));			//Коэффициенты пересчёта координат по Х
	ypGraph = -(heightY / (ymaxGraph - yminGraph));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, widthX, heightY);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);

	// заливка фона графика белым цветом
	MemDc->FillSolidRect(WinxmaxGraphc, RGB(0, 0, 0));

	CPen setka_pen;
	setka_pen.CreatePen(		//для сетки
		PS_DOT,					//пунктирная
		1,						//толщина 1 пиксель
		RGB(71, 74, 81));			//цвет  grey

	CPen osi_pen;
	osi_pen.CreatePen(		//для сетки
		PS_SOLID,				//сплошная линия
		3,						//толщина 3 пикселя
		RGB(255, 255, 255));			//цвет white

	// вывод на экран
	WinDc->BitBlt(0, 0, widthX, heightY, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void CTriangGalerkinDlg::DrawTriang(vector<Dots> rand, vector<Triangle> treug, CDC* WinDc, CRect Winxmax)
{
	//ГРАФИК СИГНАЛА
	xminGraph = sverchX_1 - 0.5;
	xmaxGraph = sverchX_3 + 0.5;
	yminGraph = sverchY_1 - 0.5;			//минимальное значение y
	ymaxGraph = sverchY_2 + 0.5;

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double widthX = Winxmax.Width();
	double heightY = Winxmax.Height();
	xpGraph = (widthX / (xmaxGraph - xminGraph));			//Коэффициенты пересчёта координат по Х
	ypGraph = -(heightY / (ymaxGraph - yminGraph));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, widthX, heightY);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);

	// заливка фона графика black цветом
	MemDc->FillSolidRect(Winxmax, RGB(0, 0, 0));

	CPen graph_pen;
	graph_pen.CreatePen(
		PS_SOLID,				//сплошная линия
		-1,						//толщина 2 пикселя
		RGB(128, 0, 128));			//цвет blue

	CPen graph_pen1;
	graph_pen1.CreatePen(
		PS_SOLID,				//сплошная линия
		5,						//толщина 2 пикселя
		RGB(0, 100, 0));			//цвет blue

	CPen north_pen;
	north_pen.CreatePen(
		PS_SOLID,				//сплошная линия
		5,						//толщина 2 пикселя
		RGB(0, 0, 255));			//цвет blue

	CPen south_pen;
	south_pen.CreatePen(
		PS_SOLID,				//сплошная линия
		5,						//толщина 2 пикселя
		RGB(255, 0, 0));			//цвет blue

	CPen gran_pen;
	gran_pen.CreatePen(
		PS_SOLID,				//сплошная линия
		5,						//толщина 2 пикселя
		RGB(255, 255, 0));			//цвет blue

	CPen graph_potenc;
	graph_potenc.CreatePen(
		PS_SOLID,				//сплошная линия
		5,						//толщина 2 пикселя
		RGB(0, 255, 255));			//цвет blue

	// отрисовка
	double difX = 0.3;
	double difY = 0.3;

	for (int i = 0; i < treug.size(); i++)
	{
		MemDc->SelectObject(&graph_pen);

		MemDc->MoveTo(DOTSGRAPH(treug[i].A.x, treug[i].A.y));
		MemDc->LineTo(DOTSGRAPH(treug[i].B.x, treug[i].B.y));
		MemDc->LineTo(DOTSGRAPH(treug[i].C.x, treug[i].C.y));
		MemDc->LineTo(DOTSGRAPH(treug[i].A.x, treug[i].A.y));
	}

	for (int i = 0; i < rand.size(); i++)
	{
		if (rand[i].north1 || rand[i].north2)
		{
			MemDc->SelectObject(&north_pen);
			MemDc->Ellipse(DOTSGRAPH(rand[i].x - difX, rand[i].y - difY),
				DOTSGRAPH(rand[i].x + difX, rand[i].y + difY));
		}

		if (rand[i].south1 || rand[i].south2)
		{
			MemDc->SelectObject(&south_pen);
			MemDc->Ellipse(DOTSGRAPH(rand[i].x - difX, rand[i].y - difY),
				DOTSGRAPH(rand[i].x + difX, rand[i].y + difY));
		}

		if (rand[i].gran)
		{
			MemDc->SelectObject(&gran_pen);
			MemDc->Ellipse(DOTSGRAPH(rand[i].x - difX, rand[i].y - difY),
				DOTSGRAPH(rand[i].x + difX, rand[i].y + difY));
		}

		if (rand[i].sv || rand[i].dr)
		{
			MemDc->SelectObject(&graph_pen1);
			MemDc->Ellipse(DOTSGRAPH(rand[i].x - difX, rand[i].y - difY),
				DOTSGRAPH(rand[i].x + difX, rand[i].y + difY));
		}

		if (potenc_draw.GetCheck() == BST_CHECKED)
		{
			MemDc->SetTextColor(RGB(0, 255, 255));
			CString str;
			str.Format(_T("%.1f"), rand[i].potenc);
			MemDc->SetBkMode(0);
			MemDc->TextOutW(DOTSGRAPH(rand[i].x, rand[i].y), str);
		}
	}

	// вывод на экран
	WinDc->BitBlt(0, 0, widthX, heightY, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void CTriangGalerkinDlg::DrawForce(vector<vector<Dots>> line, vector<Dots> rand, vector<Triangle> treug, CDC* WinDc, CRect Winxmax)
{
	//ГРАФИК СИГНАЛА
	xminGraph = sverchX_1 - 0.5;
	xmaxGraph = sverchX_3 + 0.5;
	yminGraph = sverchY_1 - 0.5;			//минимальное значение y
	ymaxGraph = sverchY_2 + 0.5;

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double widthX = Winxmax.Width();
	double heightY = Winxmax.Height();
	xpGraph = (widthX / (xmaxGraph - xminGraph));			//Коэффициенты пересчёта координат по Х
	ypGraph = -(heightY / (ymaxGraph - yminGraph));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, widthX, heightY);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);

	// заливка фона графика black цветом
	MemDc->FillSolidRect(Winxmax, RGB(0, 0, 0));

	CPen graph_pen;
	graph_pen.CreatePen(
		PS_SOLID,				//сплошная линия
		-1,						//толщина 2 пикселя
		RGB(128, 0, 128));			//цвет blue

	CPen graph_pen1;
	graph_pen1.CreatePen(
		PS_SOLID,				//сплошная линия
		2,						//толщина 2 пикселя
		RGB(0, 100, 0));			//цвет blue

	CPen north_pen;
	north_pen.CreatePen(
		PS_SOLID,				//сплошная линия
		2,						//толщина 2 пикселя
		RGB(0, 0, 255));			//цвет blue

	CPen south_pen;
	south_pen.CreatePen(
		PS_SOLID,				//сплошная линия
		2,						//толщина 2 пикселя
		RGB(255, 0, 0));			//цвет blue

	CPen gran_pen;
	gran_pen.CreatePen(
		PS_SOLID,				//сплошная линия
		2,						//толщина 2 пикселя
		RGB(255, 255, 0));			//цвет blue

	CPen force_pen;
	force_pen.CreatePen(
		PS_SOLID,				//сплошная линия
		3,						//толщина 2 пикселя
		RGB(0, 255, 0));			//цвет blue

	// отрисовка
	double difX = 0.3;
	double difY = 0.3;

	for (int i = 0; i < treug.size(); i++)
	{
		MemDc->SelectObject(&graph_pen);

		MemDc->MoveTo(DOTSGRAPH(treug[i].A.x, treug[i].A.y));
		MemDc->LineTo(DOTSGRAPH(treug[i].B.x, treug[i].B.y));
		MemDc->LineTo(DOTSGRAPH(treug[i].C.x, treug[i].C.y));
		MemDc->LineTo(DOTSGRAPH(treug[i].A.x, treug[i].A.y));
	}

	for (int i = 0; i < rand.size(); i++)
	{
		if (rand[i].north1 || rand[i].north2)
		{
			MemDc->SelectObject(&north_pen);
			MemDc->Ellipse(DOTSGRAPH(rand[i].x - difX, rand[i].y - difY),
				DOTSGRAPH(rand[i].x + difX, rand[i].y + difY));
		}

		if (rand[i].south1 || rand[i].south2)
		{
			MemDc->SelectObject(&south_pen);
			MemDc->Ellipse(DOTSGRAPH(rand[i].x - difX, rand[i].y - difY),
				DOTSGRAPH(rand[i].x + difX, rand[i].y + difY));
		}

		if (rand[i].gran)
		{
			MemDc->SelectObject(&gran_pen);
			MemDc->Ellipse(DOTSGRAPH(rand[i].x - difX, rand[i].y - difY),
				DOTSGRAPH(rand[i].x + difX, rand[i].y + difY));
		}

		if (rand[i].sv || rand[i].dr)
		{
			MemDc->SelectObject(&graph_pen1);
			MemDc->Ellipse(DOTSGRAPH(rand[i].x - difX, rand[i].y - difY),
				DOTSGRAPH(rand[i].x + difX, rand[i].y + difY));
		}
	}

	MemDc->SelectObject(&force_pen);
	for (int i = 0; i < line.size(); i++)
	{
		MemDc->MoveTo(DOTSGRAPH(line[i][0].x, line[i][0].y));
		for (int j = 0; j < line[i].size(); j++)
		{
			MemDc->LineTo(DOTSGRAPH(line[i][j].x, line[i][j].y));
		}
	}

	// вывод на экран
	WinDc->BitBlt(0, 0, widthX, heightY, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

void CTriangGalerkinDlg::DrawEqui(vector<vector<Dots>> line, vector<Dots> rand, vector<Triangle> treug, CDC* WinDc, CRect Winxmax)
{
	//ГРАФИК СИГНАЛА
	xminGraph = sverchX_1 - 0.5;
	xmaxGraph = sverchX_3 + 0.5;
	yminGraph = sverchY_1 - 0.5;			//минимальное значение y
	ymaxGraph = sverchY_2 + 0.5;

	// создание контекста устройства
	CBitmap bmp;
	CDC* MemDc;
	MemDc = new CDC;
	MemDc->CreateCompatibleDC(WinDc);

	double widthX = Winxmax.Width();
	double heightY = Winxmax.Height();
	xpGraph = (widthX / (xmaxGraph - xminGraph));			//Коэффициенты пересчёта координат по Х
	ypGraph = -(heightY / (ymaxGraph - yminGraph));			//Коэффициенты пересчёта координат по У

	bmp.CreateCompatibleBitmap(WinDc, widthX, heightY);
	CBitmap* pBmp = (CBitmap*)MemDc->SelectObject(&bmp);

	// заливка фона графика black цветом
	MemDc->FillSolidRect(Winxmax, RGB(0, 0, 0));

	CPen graph_pen;
	graph_pen.CreatePen(
		PS_SOLID,				//сплошная линия
		-1,						//толщина 2 пикселя
		RGB(128, 0, 128));			//цвет blue

	CPen graph_pen1;
	graph_pen1.CreatePen(
		PS_SOLID,				//сплошная линия
		3,						//толщина 2 пикселя
		RGB(0, 100, 0));			//цвет blue

	CPen north_pen;
	north_pen.CreatePen(
		PS_SOLID,				//сплошная линия
		3,						//толщина 2 пикселя
		RGB(0, 0, 255));			//цвет blue

	CPen south_pen;
	south_pen.CreatePen(
		PS_SOLID,				//сплошная линия
		3,						//толщина 2 пикселя
		RGB(255, 0, 0));			//цвет blue

	CPen gran_pen;
	gran_pen.CreatePen(
		PS_SOLID,				//сплошная линия
		3,						//толщина 2 пикселя
		RGB(255, 255, 0));			//цвет blue

	CPen equi_pen;
	equi_pen.CreatePen(
		PS_SOLID,				//сплошная линия
		2,						//толщина 2 пикселя
		RGB(255, 255, 255));			//цвет blue

	// отрисовка
	double difX = 0.3;
	double difY = 0.3;

	for (int i = 0; i < treug.size(); i++)
	{
		MemDc->SelectObject(&graph_pen);

		MemDc->MoveTo(DOTSGRAPH(treug[i].A.x, treug[i].A.y));
		MemDc->LineTo(DOTSGRAPH(treug[i].B.x, treug[i].B.y));
		MemDc->LineTo(DOTSGRAPH(treug[i].C.x, treug[i].C.y));
		MemDc->LineTo(DOTSGRAPH(treug[i].A.x, treug[i].A.y));
	}

	for (int i = 0; i < rand.size(); i++)
	{
		if (rand[i].north1 || rand[i].north2)
		{
			MemDc->SelectObject(&north_pen);
			MemDc->Ellipse(DOTSGRAPH(rand[i].x - difX, rand[i].y - difY),
				DOTSGRAPH(rand[i].x + difX, rand[i].y + difY));
		}

		if (rand[i].south1 || rand[i].south2)
		{
			MemDc->SelectObject(&south_pen);
			MemDc->Ellipse(DOTSGRAPH(rand[i].x - difX, rand[i].y - difY),
				DOTSGRAPH(rand[i].x + difX, rand[i].y + difY));
		}

		if (rand[i].gran)
		{
			MemDc->SelectObject(&gran_pen);
			MemDc->Ellipse(DOTSGRAPH(rand[i].x - difX, rand[i].y - difY),
				DOTSGRAPH(rand[i].x + difX, rand[i].y + difY));
		}

		if (rand[i].sv || rand[i].dr)
		{
			MemDc->SelectObject(&graph_pen1);
			MemDc->Ellipse(DOTSGRAPH(rand[i].x - difX, rand[i].y - difY),
				DOTSGRAPH(rand[i].x + difX, rand[i].y + difY));
		}
	}

	MemDc->SelectObject(&equi_pen);
	for (int i = 0; i < line.size(); i++)
	{
		MemDc->MoveTo(DOTSGRAPH(line[i][0].x, line[i][0].y));
		for (int j = 0; j < line[i].size(); j++)
		{
			MemDc->LineTo(DOTSGRAPH(line[i][j].x, line[i][j].y));
		}
	}

	// вывод на экран
	WinDc->BitBlt(0, 0, widthX, heightY, MemDc, 0, 0, SRCCOPY);
	delete MemDc;
}

bool CTriangGalerkinDlg::enters(int npol, vector<Dots> kvadr, double x, double y)
{
	bool c = false;
	for (int i = 0, j = npol - 1; i < npol; j = i++)
	{
		if ((
			(kvadr[i].y < kvadr[j].y) && (kvadr[i].y < y) && (y < kvadr[j].y) &&
			((kvadr[j].y - kvadr[i].y) * (x - kvadr[i].x) > (kvadr[j].x - kvadr[i].x) * (y - kvadr[i].y))
			) || (
				(kvadr[i].y > kvadr[j].y) && (kvadr[j].y < y) && (y < kvadr[i].y) &&
				((kvadr[j].y - kvadr[i].y) * (x - kvadr[i].x) < (kvadr[j].x - kvadr[i].x) * (y - kvadr[i].y))
				))
			c = !c;
	}

	return c;
}

bool CTriangGalerkinDlg::UpDown(double x, double y, double x1, double y1, double x2, double y2)
{
	bool c = false;

	double chisl1 = x - x1;
	double chisl2 = y - y1;
	double znam1 = x2 - x1;
	double znam2 = y2 - y1;

	double itog = (chisl1 / znam1) - (chisl2 / znam2);

	if (itog >= 0)
	{
		c = !c;
	}

	return c;
}

void CTriangGalerkinDlg::CalcDyrky()
{
	Dots help;

	//точки рабочей области
	double helpX = 0;
	double helpY = 0;

	bool vkl = false;
	double X, Y = 0;
	double poluWidht = width1 / 2.;
	double poluHeight = height1 / 2.;
	double helper = (angle1 * 3.1415926) / 180;

	double X1 = centerX_1 - poluWidht;
	double Y1 = centerY_1 - poluHeight;
	double X2 = centerX_1 - poluWidht;
	double Y2 = centerY_1 + poluHeight;
	double X3 = centerX_1 + poluWidht;
	double Y3 = centerY_1 + poluHeight;
	double X4 = centerX_1 + poluWidht;
	double Y4 = centerY_1 - poluHeight;

	Dots middle1;
	Dots middle2;
	middle1.x = centerX_1 + (X1 - centerX_1) * cos(helper) + (centerY_1 - centerY_1) * sin(helper);
	middle1.y = centerY_1 + (centerY_1 - centerY_1) * cos(helper) - (X1 - centerX_1) * sin(helper);
	middle2.x = centerX_1 + (X3 - centerX_1) * cos(helper) + (centerY_1 - centerY_1) * sin(helper);
	middle2.y = centerY_1 + (centerY_1 - centerY_1) * cos(helper) - (X3 - centerX_1) * sin(helper);

	bool pointUpDown;
	//точки дырок
	for (double j = Y1; j < Y2; j += step)
	{
		helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;
		X = X1 + helpX;
		Y = j + helpY;
		help.x = centerX_1 + (X - centerX_1) * cos(helper) + (Y - centerY_1) * sin(helper);
		help.y = centerY_1 + (Y - centerY_1) * cos(helper) - (X - centerX_1) * sin(helper);

		square1.push_back(help);
	}

	for (double i = X2; i < X3; i += step)
	{
		helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;
		X = i + helpX;
		Y = Y2 + helpY;
		help.x = centerX_1 + (X - centerX_1) * cos(helper) + (Y - centerY_1) * sin(helper);
		help.y = centerY_1 + (Y - centerY_1) * cos(helper) - (X - centerX_1) * sin(helper);

		square1.push_back(help);
	}

	for (double j = Y3; j > Y4; j -= step)
	{
		helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;
		X = X3 + helpX;
		Y = j + helpY;
		help.x = centerX_1 + (X - centerX_1) * cos(helper) + (Y - centerY_1) * sin(helper);
		help.y = centerY_1 + (Y - centerY_1) * cos(helper) - (X - centerX_1) * sin(helper);

		square1.push_back(help);
	}

	for (double i = X4; i > X1; i -= step)
	{
		helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;
		X = i + helpX;
		Y = Y4 + helpY;
		help.x = centerX_1 + (X - centerX_1) * cos(helper) + (Y - centerY_1) * sin(helper);
		help.y = centerY_1 + (Y - centerY_1) * cos(helper) - (X - centerX_1) * sin(helper);

		square1.push_back(help);
	}

	for (int i = 0; i < square1.size(); i++)
	{
		pointUpDown = UpDown(square1[i].x, square1[i].y, middle1.x, middle1.y, middle2.x, middle2.y);

		if (pointUpDown)
		{
			square1[i].dr = false;
			square1[i].north1 = true;
			square1[i].south1 = false;
			square1[i].potenc = -U_dyrki;
		}
		else
		{
			square1[i].dr = false;
			square1[i].south1 = true;
			square1[i].north1 = false;
			square1[i].potenc = U_dyrki;
		}
	}

	poluWidht = width2 / 2.;
	poluHeight = height2 / 2.;
	helper = (angle2 * 3.1415926) / 180;

	X1 = centerX_2 - poluWidht;
	Y1 = centerY_2 - poluHeight;
	X2 = centerX_2 - poluWidht;
	Y2 = centerY_2 + poluHeight;
	X3 = centerX_2 + poluWidht;
	Y3 = centerY_2 + poluHeight;
	X4 = centerX_2 + poluWidht;
	Y4 = centerY_2 - poluHeight;

	middle1.x = centerX_2 + (X1 - centerX_2) * cos(helper) + (centerY_2 - centerY_2) * sin(helper);
	middle1.y = centerY_2 + (centerY_2 - centerY_2) * cos(helper) - (X1 - centerX_2) * sin(helper);
	middle2.x = centerX_2 + (X3 - centerX_2) * cos(helper) + (centerY_2 - centerY_2) * sin(helper);
	middle2.y = centerY_2 + (centerY_2 - centerY_2) * cos(helper) - (X3 - centerX_2) * sin(helper);

	for (double j = Y1; j < Y2; j += step)
	{
		helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;
		X = X1 + helpX;
		Y = j + helpY;
		help.x = centerX_2 + (X - centerX_2) * cos(helper) + (Y - centerY_2) * sin(helper);
		help.y = centerY_2 + (Y - centerY_2) * cos(helper) - (X - centerX_2) * sin(helper);

		square2.push_back(help);
	}

	for (double i = X2; i < X3; i += step)
	{
		helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;
		X = i + helpX;
		Y = Y2 + helpY;
		help.x = centerX_2 + (X - centerX_2) * cos(helper) + (Y - centerY_2) * sin(helper);
		help.y = centerY_2 + (Y - centerY_2) * cos(helper) - (X - centerX_2) * sin(helper);

		square2.push_back(help);
	}

	for (double j = Y3; j > Y4; j -= step)
	{
		helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;
		X = X3 + helpX;
		Y = j + helpY;
		help.x = centerX_2 + (X - centerX_2) * cos(helper) + (Y - centerY_2) * sin(helper);
		help.y = centerY_2 + (Y - centerY_2) * cos(helper) - (X - centerX_2) * sin(helper);

		square2.push_back(help);
	}

	for (double i = X4; i > X1; i -= step)
	{
		helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;
		X = i + helpX;
		Y = Y4 + helpY;
		help.x = centerX_2 + (X - centerX_2) * cos(helper) + (Y - centerY_2) * sin(helper);
		help.y = centerY_2 + (Y - centerY_2) * cos(helper) - (X - centerX_2) * sin(helper);

		square2.push_back(help);
	}

	for (int i = 0; i < square2.size(); i++)
	{
		pointUpDown = UpDown(square2[i].x, square2[i].y, middle1.x, middle1.y, middle2.x, middle2.y);

		if (pointUpDown)
		{
			square2[i].dr = false;
			square2[i].north2 = true;
			square2[i].south2 = false;
			square2[i].potenc = -U_dyrki;
		}
		else
		{
			square2[i].dr = false;
			square2[i].south2 = true;
			square2[i].north2 = false;
			square2[i].potenc = U_dyrki;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	//чуть больше прямоугольнички

	//точки рабочей области
	helpX = 0;
	helpY = 0;

	X, Y = 0;
	poluWidht = (width1 + Rand) / 2.;
	poluHeight = (height1 + Rand) / 2.;
	helper = (angle1 * 3.1415926) / 180;

	X1 = centerX_1 - poluWidht;
	Y1 = centerY_1 - poluHeight;
	X2 = centerX_1 - poluWidht;
	Y2 = centerY_1 + poluHeight;
	X3 = centerX_1 + poluWidht;
	Y3 = centerY_1 + poluHeight;
	X4 = centerX_1 + poluWidht;
	Y4 = centerY_1 - poluHeight;

	//точки дырок
	for (double j = Y1; j < Y2; j += step)
	{
		helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;
		X = X1 + helpX;
		Y = j + helpY;
		help.x = centerX_1 + (X - centerX_1) * cos(helper) + (Y - centerY_1) * sin(helper);
		help.y = centerY_1 + (Y - centerY_1) * cos(helper) - (X - centerX_1) * sin(helper);
		help.del = true;
		squareHelp1.push_back(help);
	}

	for (double i = X2; i < X3; i += step)
	{
		helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;
		X = i + helpX;
		Y = Y2 + helpY;
		help.x = centerX_1 + (X - centerX_1) * cos(helper) + (Y - centerY_1) * sin(helper);
		help.y = centerY_1 + (Y - centerY_1) * cos(helper) - (X - centerX_1) * sin(helper);
		help.del = true;
		squareHelp1.push_back(help);
	}

	for (double j = Y3; j > Y4; j -= step)
	{
		helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;
		X = X3 + helpX;
		Y = j + helpY;
		help.x = centerX_1 + (X - centerX_1) * cos(helper) + (Y - centerY_1) * sin(helper);
		help.y = centerY_1 + (Y - centerY_1) * cos(helper) - (X - centerX_1) * sin(helper);
		help.del = true;
		squareHelp1.push_back(help);
	}

	for (double i = X4; i > X1; i -= step)
	{
		helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;
		X = i + helpX;
		Y = Y4 + helpY;
		help.x = centerX_1 + (X - centerX_1) * cos(helper) + (Y - centerY_1) * sin(helper);
		help.y = centerY_1 + (Y - centerY_1) * cos(helper) - (X - centerX_1) * sin(helper);
		help.del = true;
		squareHelp1.push_back(help);
	}

	poluWidht = (width2 + Rand) / 2.;
	poluHeight = (height2 + Rand) / 2.;
	helper = (angle2 * 3.1415926) / 180;

	X1 = centerX_2 - poluWidht;
	Y1 = centerY_2 - poluHeight;
	X2 = centerX_2 - poluWidht;
	Y2 = centerY_2 + poluHeight;
	X3 = centerX_2 + poluWidht;
	Y3 = centerY_2 + poluHeight;
	X4 = centerX_2 + poluWidht;
	Y4 = centerY_2 - poluHeight;

	for (double j = Y1; j < Y2; j += step)
	{
		helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;
		X = X1 + helpX;
		Y = j + helpY;
		help.x = centerX_2 + (X - centerX_2) * cos(helper) + (Y - centerY_2) * sin(helper);
		help.y = centerY_2 + (Y - centerY_2) * cos(helper) - (X - centerX_2) * sin(helper);
		help.del = true;
		squareHelp2.push_back(help);
	}

	for (double i = X2; i < X3; i += step)
	{
		helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;
		X = i + helpX;
		Y = Y2 + helpY;
		help.x = centerX_2 + (X - centerX_2) * cos(helper) + (Y - centerY_2) * sin(helper);
		help.y = centerY_2 + (Y - centerY_2) * cos(helper) - (X - centerX_2) * sin(helper);
		help.del = true;
		squareHelp2.push_back(help);
	}

	for (double j = Y3; j > Y4; j -= step)
	{
		helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;
		X = X3 + helpX;
		Y = j + helpY;
		help.x = centerX_2 + (X - centerX_2) * cos(helper) + (Y - centerY_2) * sin(helper);
		help.y = centerY_2 + (Y - centerY_2) * cos(helper) - (X - centerX_2) * sin(helper);
		help.del = true;
		squareHelp2.push_back(help);
	}

	for (double i = X4; i > X1; i -= step)
	{
		helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;
		X = i + helpX;
		Y = Y4 + helpY;
		help.x = centerX_2 + (X - centerX_2) * cos(helper) + (Y - centerY_2) * sin(helper);
		help.y = centerY_2 + (Y - centerY_2) * cos(helper) - (X - centerX_2) * sin(helper);
		help.del = true;
		squareHelp2.push_back(help);
	}
}

void CTriangGalerkinDlg::Sverch(vector<Dots>& vec)
{
	Dots help;
	// 4 точки  границы
	help.x = sverchX_1;
	help.y = sverchY_1;
	help.sv = true;
	vec.push_back(help);
	help.x = sverchX_2;
	help.y = sverchY_2;
	help.sv = true;
	vec.push_back(help);
	help.x = sverchX_3;
	help.y = sverchY_3;
	help.sv = true;
	vec.push_back(help);
	help.x = sverchX_4;
	help.y = sverchY_4;
	help.sv = true;
	vec.push_back(help);

	for (int i = 0; i < square1.size(); i++)
	{
		vec.push_back(square1[i]);
	}

	for (int i = 0; i < square2.size(); i++)
	{
		vec.push_back(square2[i]);
	}
}

double CTriangGalerkinDlg::Described(Dots iDots, Dots jDots, Dots kDots, double& XC, double& YC)
{
	double chislitRx = (jDots.x * jDots.x - iDots.x * iDots.x + jDots.y * jDots.y - iDots.y * iDots.y) *
		(kDots.y - iDots.y) - (kDots.x * kDots.x - iDots.x * iDots.x + kDots.y * kDots.y -
			iDots.y * iDots.y) * (jDots.y - iDots.y);
	double znamenatRx = (jDots.x - iDots.x) * (kDots.y - iDots.y) -
		(kDots.x - iDots.x) * (jDots.y - iDots.y);
	XC = (chislitRx / znamenatRx);
	XC /= 2.;

	double chislitRy = (kDots.x * kDots.x - iDots.x * iDots.x + kDots.y * kDots.y - iDots.y * iDots.y) *
		(jDots.x - iDots.x) - (jDots.x * jDots.x - iDots.x * iDots.x + jDots.y * jDots.y -
			iDots.y * iDots.y) * (kDots.x - iDots.x);
	double znamenatRy = (jDots.x - iDots.x) * (kDots.y - iDots.y) -
		(kDots.x - iDots.x) * (jDots.y - iDots.y);
	YC = (chislitRy / znamenatRy);
	YC /= 2.;

	double opis = sqrt((XC - iDots.x) * (XC - iDots.x) + (YC - iDots.y) * (YC - iDots.y));

	return opis;
}

void  CTriangGalerkinDlg::TriangFunc(int pointN, vector<Dots>& point, vector<Triangle>& triangleABC)
{
	Triangle trngl;
	double Xc = 0.;
	double Yc = 0.;
	double opisR = 0.;
	double help = 0.;
	bool need_to_add_triangle = true;
	triangleABC.clear();

	for (int i = 0; i < pointN - 2; i++)
	{
		for (int j = i + 1; j < pointN - 1; j++)
		{
			for (int k = j + 1; k < pointN; k++)
			{
				opisR = Described(point[i], point[j], point[k], Xc, Yc);
				need_to_add_triangle = true;

				for (int m = 0; m < pointN; m++)
				{
					if (m != i && m != j && m != k)
					{
						help = (point[m].x - Xc) * (point[m].x - Xc) +
							(point[m].y - Yc) * (point[m].y - Yc);

						if (help <= opisR * opisR)
						{
							need_to_add_triangle = false;
							break;
						}
					}
				}

				if (need_to_add_triangle)
				{
					trngl.A = point[i];
					trngl.B = point[j];
					trngl.C = point[k];

					triangleABC.push_back(trngl);
				}
			}
		}
	}
}

void CTriangGalerkinDlg::Delete(vector<Triangle>& vecTriang)
{
	Dots sredAB;
	Dots sredBC;
	Dots sredCA;

	for (int i = 0; i < vecTriang.size(); i++)
	{
		if (vecTriang[i].A.sv || vecTriang[i].B.sv || vecTriang[i].C.sv)
		{
			vecTriang.erase(vecTriang.begin() + i);
			i--;
		}
	}

	bool help1 = false;
	bool help2 = false;
	bool help3 = false;

	for (int i = 0; i < vecTriang.size(); i++)
	{
		sredAB.x = (vecTriang[i].A.x + vecTriang[i].B.x) / 2.0;
		sredBC.x = (vecTriang[i].B.x + vecTriang[i].C.x) / 2.0;
		sredCA.x = (vecTriang[i].C.x + vecTriang[i].A.x) / 2.0;

		sredAB.y = (vecTriang[i].A.y + vecTriang[i].B.y) / 2.0;
		sredBC.y = (vecTriang[i].B.y + vecTriang[i].C.y) / 2.0;
		sredCA.y = (vecTriang[i].C.y + vecTriang[i].A.y) / 2.0;

		help1 = enters(squareHelp1.size(), squareHelp1, sredAB.x, sredAB.y);
		help2 = enters(squareHelp1.size(), squareHelp1, sredBC.x, sredBC.y);
		help3 = enters(squareHelp1.size(), squareHelp1, sredCA.x, sredCA.y);

		if ((help1 && help2) || (help1 && help3) || (help3 && help2))
		{
			vecTriang.erase(vecTriang.begin() + i);
			i--;
		}
	}

	for (int i = 0; i < vecTriang.size(); i++)
	{
		sredAB.x = (vecTriang[i].A.x + vecTriang[i].B.x) / 2.0;
		sredBC.x = (vecTriang[i].B.x + vecTriang[i].C.x) / 2.0;
		sredCA.x = (vecTriang[i].C.x + vecTriang[i].A.x) / 2.0;

		sredAB.y = (vecTriang[i].A.y + vecTriang[i].B.y) / 2.0;
		sredBC.y = (vecTriang[i].B.y + vecTriang[i].C.y) / 2.0;
		sredCA.y = (vecTriang[i].C.y + vecTriang[i].A.y) / 2.0;

		help1 = enters(squareHelp2.size(), squareHelp2, sredAB.x, sredAB.y);
		help2 = enters(squareHelp2.size(), squareHelp2, sredBC.x, sredBC.y);
		help3 = enters(squareHelp2.size(), squareHelp2, sredCA.x, sredCA.y);

		if ((help1 && help2) || (help1 && help3) || (help3 && help2))
		{
			vecTriang.erase(vecTriang.begin() + i);
			i--;
		}
	}

	for (int i = 0; i < PointsSverch.size(); i++)
	{
		if (PointsSverch[i].sv == true)
		{
			PointsSverch.erase(PointsSverch.begin() + i);
			i--;
		}
	}
}

void CTriangGalerkinDlg::randPointFunc(vector<Dots>& vec)
{
	randPoints.clear();
	Dots helper;
	for (double i = oblY_1; i <= oblY_2; i += step)
	{
		double helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		double helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;

		helper.x = oblX_1 + helpX;
		helper.y = i + helpY;
		helper.gran = true;
		helper.dr = false;
		helper.potenc = U_gran;

		vec.push_back(helper);
	}

	for (double i = oblX_2; i <= oblX_3; i += step)
	{
		double helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		double helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;

		helper.x = i + helpX;
		helper.y = oblY_2 + helpY;
		helper.gran = true;
		helper.dr = false;
		helper.potenc = U_gran;

		vec.push_back(helper);
	}

	for (double i = oblY_3; i >= oblY_4; i -= step)
	{
		double helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		double helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;

		helper.x = oblX_3 + helpX;
		helper.y = i + helpY;
		helper.gran = true;
		helper.dr = false;
		helper.potenc = U_gran;

		vec.push_back(helper);
	}

	for (double i = oblX_4; i >= oblX_1; i -= step)
	{
		double helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
		double helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;

		helper.x = i - helpX;
		helper.y = oblY_4 + helpY;
		helper.gran = true;
		helper.dr = false;
		helper.potenc = U_gran;

		vec.push_back(helper);
	}

	for (double i = step; i < (oblX_3 - oblX_2); i += step)
	{
		for (double j = step; j < (oblY_2 - oblY_1); j += step)
		{
			double helpX = minRand + ((double)rand() / RAND_MAX) * maxRand;
			double helpY = minRand + ((double)rand() / RAND_MAX) * maxRand;

			helper.x = (oblX_2 + i) + helpX;
			helper.y = (oblY_1 + j) + helpY;
			helper.dr = true;
			helper.gran = false;

			vec.push_back(helper);
		}
	}
}

void CTriangGalerkinDlg::AnimationInformation()
{
	PointsSverch.clear();	recTriang.clear();
	vector<Dots> workPoint;
	vector<Triangle> recTrianghelp;

	//vector random points
	randPointFunc(randPoints);

	for (int i = 0; i < randPoints.size(); i++)
	{
		bool connect = enters(squareHelp1.size(), squareHelp1, randPoints[i].x, randPoints[i].y);
		if (connect)
		{
			randPoints.erase(randPoints.begin() + i);
			i--;
		}
	}

	for (int i = 0; i < randPoints.size(); i++)
	{
		bool connect = enters(squareHelp2.size(), squareHelp2, randPoints[i].x, randPoints[i].y);
		if (connect)
		{
			randPoints.erase(randPoints.begin() + i);
			i--;
		}
	}

	//vector points sverhstruct
	Sverch(PointsSverch);
	TriangFunc(PointsSverch.size(), PointsSverch, recTriang);

	Triangle trngl;
	double Xc = 0.;
	double Yc = 0.;
	double opisR = 0.;
	double help = 0.;
	double eps = 1.e-6;
	workPoint.clear();

	for (int m = 0; m < randPoints.size(); m++)
	{
		workPoint.push_back(randPoints[m]);
		PointsSverch.push_back(randPoints[m]);

		for (int i = 0; i < recTriang.size(); i++)
		{
			opisR = Described(recTriang[i].A, recTriang[i].B, recTriang[i].C, Xc, Yc);

			help = (randPoints[m].x - Xc) * (randPoints[m].x - Xc) + (randPoints[m].y - Yc) * (randPoints[m].y - Yc);

			if (help <= opisR * opisR)
			{
				bool add = true;
				for (int g = 0; g < workPoint.size(); g++)
				{
					if (abs(workPoint[g].x - recTriang[i].A.x) < eps && abs(workPoint[g].y - recTriang[i].A.y) < eps)
					{
						add = false;
						break;
					}
				}
				if (add) workPoint.push_back(recTriang[i].A);

				add = true;
				for (int g = 0; g < workPoint.size(); g++)
				{
					if (abs(workPoint[g].x - recTriang[i].B.x) < eps && abs(workPoint[g].y - recTriang[i].B.y) < eps)
					{
						add = false;
						break;
					}
				}
				if (add) workPoint.push_back(recTriang[i].B);

				add = true;
				for (int g = 0; g < workPoint.size(); g++)
				{
					if (abs(workPoint[g].x - recTriang[i].C.x) < eps && abs(workPoint[g].y - recTriang[i].C.y) < eps)
					{
						add = false;
						break;
					}
				}
				if (add) workPoint.push_back(recTriang[i].C);

				recTriang.erase(recTriang.begin() + i);
				i--;
			}
		}

		TriangFunc(workPoint.size(), workPoint, recTrianghelp);

		for (int g = 0; g < recTrianghelp.size(); g++)
		{
			if (abs(randPoints[m].x - recTrianghelp[g].A.x) < eps && abs(randPoints[m].y - recTrianghelp[g].A.y) < eps ||
				abs(randPoints[m].x - recTrianghelp[g].B.x) < eps && abs(randPoints[m].y - recTrianghelp[g].B.y) < eps ||
				abs(randPoints[m].x - recTrianghelp[g].C.x) < eps && abs(randPoints[m].y - recTrianghelp[g].C.y) < eps)
			{
				recTriang.push_back(recTrianghelp[g]);
			}
		}

		workPoint.clear();
		recTrianghelp.clear();
	}

	DrawTriang(PointsSverch, recTriang, PicDcGraph, PicGraph);

	workPoint.clear();
	recTrianghelp.clear();
}

void CTriangGalerkinDlg::vertexUp(Dots pt, Triangle triang, Triangle& triang_out)
{
	double eps = 1.e-6;
	if (abs(triang.A.x - pt.x) < eps && abs(triang.A.y - pt.y) < eps)
	{
		triang.A.z = 1;
		triang.B.z = 0;
		triang.C.z = 0;
	}

	if (abs(triang.B.x - pt.x) < eps && abs(triang.B.y - pt.y) < eps)
	{
		triang.A.z = 0;
		triang.B.z = 1;
		triang.C.z = 0;
	}

	if (abs(triang.C.x - pt.x) < eps && abs(triang.C.y - pt.y) < eps)
	{
		triang.A.z = 0;
		triang.B.z = 0;
		triang.C.z = 1;
	}

	triang_out = triang;
}

void CTriangGalerkinDlg::DotsNeighbours(Dots i, Dots j, vector<Triangle> dln, vector<Triangle>& neighbours)
{
	double eps = 1e-6;
	vector<Triangle> helperTriang;
	for (int k = 0; k < recTriang.size(); k++)
	{
		if (abs(dln[k].A.x - i.x) <= eps && abs(dln[k].A.y - i.y) <= eps)
		{
			helperTriang.push_back(dln[k]);
		}

		if (abs(dln[k].B.x - i.x) <= eps && abs(dln[k].B.y - i.y) <= eps)
		{
			helperTriang.push_back(dln[k]);
		}

		if (abs(dln[k].C.x - i.x) <= eps && abs(dln[k].C.y - i.y) <= eps)
		{
			helperTriang.push_back(dln[k]);
		}
	}

	if (!helperTriang.empty())
	{
		for (int c = 0; c < helperTriang.size(); c++)
		{
			if ((helperTriang[c].A.x == j.x && helperTriang[c].A.y == j.y)
				|| (helperTriang[c].B.x == j.x && helperTriang[c].B.y == j.y)
				|| (helperTriang[c].C.x == j.x && helperTriang[c].C.y == j.y))
			{
				neighbours.push_back(helperTriang[c]);
			}
		}
	}
}

double CTriangGalerkinDlg::GetS(double x1, double y1, double x2, double y2, double x3, double y3)
{
	//double triangS = abs((x1 - x3) * (y2 - y3) - (x2 - x3) * (y1 - y3));
	double triangS = abs((x2 - x1) * (y3 - y1) - ((x3 - x1) * (y2 - y1)));
	triangS /= 2.0;

	return triangS;
}

void CTriangGalerkinDlg::matrixAij(vector<Dots> vecVnutr, vector<double>& MatrAij)
{
	double eps = 1.e-6;
	for (int i = 0; i < vecVnutr.size(); i++)
	{
		for (int j = 0; j < vecVnutr.size(); j++)
		{
			double a_ij = 0.0;
			double triangS = 0;
			vector<Triangle> helperTriang;
			if (i == j)
			{
				double A = 0;
				double B = 0;

				//треугольники с точкой i
				for (int k = 0; k < recTriang.size(); k++)
				{
					if (abs(recTriang[k].A.x - vecVnutr[i].x) < eps && abs(recTriang[k].A.y - vecVnutr[i].y) < eps
						/*&& !recTriang[k].A.north && !recTriang[k].A.south && !recTriang[k].A.gran*/)
					{
						helperTriang.push_back(recTriang[k]);
					}

					if (abs(recTriang[k].B.x - vecVnutr[i].x) < eps && abs(recTriang[k].B.y - vecVnutr[i].y) < eps
						/*&& !recTriang[k].B.north && !recTriang[k].B.south && !recTriang[k].B.gran*/)
					{
						helperTriang.push_back(recTriang[k]);
					}

					if (abs(recTriang[k].C.x - vecVnutr[i].x) < eps && abs(recTriang[k].C.y - vecVnutr[i].y) < eps
						/*&& !recTriang[k].C.north && !recTriang[k].C.south && !recTriang[k].C.gran*/)
					{
						helperTriang.push_back(recTriang[k]);
					}
				}

				for (int k = 0; k < helperTriang.size(); k++)
				{
					Triangle triang_out;
					vertexUp(vecVnutr[j], helperTriang[k], triang_out);

					A = triang_out.A.y * (triang_out.B.z - triang_out.C.z) + triang_out.B.y * (triang_out.C.z - triang_out.A.z) + triang_out.C.y * (triang_out.A.z - triang_out.B.z);
					B = triang_out.A.z * (triang_out.B.x - triang_out.C.x) + triang_out.B.z * (triang_out.C.x - triang_out.A.x) + triang_out.C.z * (triang_out.A.x - triang_out.B.x);

					triangS = GetS(helperTriang[k].A.x, helperTriang[k].A.y, helperTriang[k].B.x, helperTriang[k].B.y, helperTriang[k].C.x, helperTriang[k].C.y);

					a_ij += (A * A + B * B) * triangS;
				}

				MatrAij.push_back(-a_ij);
			}

			if (i != j)
			{
				DotsNeighbours(vecVnutr[i], vecVnutr[j], recTriang, helperTriang);

				double Ai = 0;	 double Aj = 0;
				double Bi = 0;	 double Bj = 0;
				for (int k = 0; k < helperTriang.size(); k++)
				{
					Triangle triang_out1;
					vertexUp(vecVnutr[i], helperTriang[k], triang_out1);

					Ai = triang_out1.A.y * (triang_out1.B.z - triang_out1.C.z) + triang_out1.B.y * (triang_out1.C.z - triang_out1.A.z) + triang_out1.C.y * (triang_out1.A.z - triang_out1.B.z);
					Bi = triang_out1.A.z * (triang_out1.B.x - triang_out1.C.x) + triang_out1.B.z * (triang_out1.C.x - triang_out1.A.x) + triang_out1.C.z * (triang_out1.A.x - triang_out1.B.x);

					Triangle triang_out2;
					vertexUp(vecVnutr[j], helperTriang[k], triang_out2);

					Aj = triang_out2.A.y * (triang_out2.B.z - triang_out2.C.z) + triang_out2.B.y * (triang_out2.C.z - triang_out2.A.z) + triang_out2.C.y * (triang_out2.A.z - triang_out2.B.z);
					Bj = triang_out2.A.z * (triang_out2.B.x - triang_out2.C.x) + triang_out2.B.z * (triang_out2.C.x - triang_out2.A.x) + triang_out2.C.z * (triang_out2.A.x - triang_out2.B.x);

					triangS = GetS(helperTriang[k].A.x, helperTriang[k].A.y, helperTriang[k].B.x, helperTriang[k].B.y, helperTriang[k].C.x, helperTriang[k].C.y);

					a_ij += (Ai * Aj + Bi * Bj) * triangS;
				}

				MatrAij.push_back(-a_ij);
			}
		}
	}

	/*ofstream f("matr.txt");
	for (int i = 0; i < MatrAij.size(); i++)
	{
		if ((i % vecVnutr.size()) == 0)
		{
			f << endl << endl;
		}
		f << setprecision(3) << MatrAij[i] << "\t";
	}
	f.close();*/
}

void CTriangGalerkinDlg::vectorBj(vector<Dots> vecVnutr, vector<Dots> vecGran, vector<double>& vecj)
{
	vector<Triangle> helperTriang;

	for (int i = 0; i < vecVnutr.size(); i++)
	{
		double bj = 0.;
		for (int j = 0; j < vecGran.size(); j++)
		{
			double bjm = 0.;
			helperTriang.clear();
			DotsNeighbours(vecGran[j], vecVnutr[i], recTriang, helperTriang);

			double Ai = 0.;	 double Aj = 0.;
			double Bi = 0.;	 double Bj = 0.;
			double triangS = 0;
			for (int k = 0; k < helperTriang.size(); k++)
			{
				Triangle triang_out1;
				vertexUp(vecVnutr[i], helperTriang[k], triang_out1);

				Ai = triang_out1.A.y * (triang_out1.B.z - triang_out1.C.z) + triang_out1.B.y * (triang_out1.C.z - triang_out1.A.z) + triang_out1.C.y * (triang_out1.A.z - triang_out1.B.z);
				Bi = triang_out1.A.z * (triang_out1.B.x - triang_out1.C.x) + triang_out1.B.z * (triang_out1.C.x - triang_out1.A.x) + triang_out1.C.z * (triang_out1.A.x - triang_out1.B.x);

				Triangle triang_out2;
				vertexUp(vecGran[j], helperTriang[k], triang_out2);

				Aj = triang_out2.A.y * (triang_out2.B.z - triang_out2.C.z) + triang_out2.B.y * (triang_out2.C.z - triang_out2.A.z) + triang_out2.C.y * (triang_out2.A.z - triang_out2.B.z);
				Bj = triang_out2.A.z * (triang_out2.B.x - triang_out2.C.x) + triang_out2.B.z * (triang_out2.C.x - triang_out2.A.x) + triang_out2.C.z * (triang_out2.A.x - triang_out2.B.x);

				triangS = GetS(helperTriang[k].A.x, helperTriang[k].A.y, helperTriang[k].B.x, helperTriang[k].B.y, helperTriang[k].C.x, helperTriang[k].C.y);

				bjm += (Ai * Aj + Bi * Bj) * triangS;
			}

			bj += bjm * vecGran[j].potenc;
		}

		vecj.push_back(bj);
	}

	/*ofstream f("vecB.txt");
	for (int i = 0; i < vecj.size(); i++)
	{
		f << endl;
		f << setprecision(3) << vecj[i];
	}
	f.close();*/
}

void CTriangGalerkinDlg::kazf(vector<double> a, vector<double>  b, vector<double>& x, int nn, int ny)
{
	// nn - количество неизвестных;  ny - количество уравнений
	double eps = 1.e-6;
	//float s;
	int i, j, k;
	double s1, s2, fa1, t;

	vector<double> x1(nn);

	x[0] = 0.5;
	for (i = 1; i < nn; i++)
	{
		x[i] = 0.0;
	}

	s1 = s2 = 1.0;
	while (s1 > eps * s2)
	{
		for (i = 0; i < nn; i++)
		{
			x1[i] = x[i];
		}

		for (i = 0; i < ny; i++)
		{
			s1 = 0.0;
			s2 = 0.0;
			for (j = 0; j < nn; j++)
			{
				fa1 = a[i * nn + j];
				s1 += fa1 * x[j];
				s2 += fa1 * fa1;
			}

			t = (b[i] - s1) / s2;
			for (k = 0; k < nn; k++)
			{
				x[k] += a[i * nn + k] * t;
			}
		}

		s1 = 0.0;
		s2 = 0.0;
		for (i = 0; i < nn; i++)
		{
			s1 += (x[i] - x1[i]) * (x[i] - x1[i]);
			s2 += x[i] * x[i];
		}
		s1 = (double)sqrt(s1);
		s2 = (double)sqrt(s2);
	}

	x1.clear();
}

void CTriangGalerkinDlg::intersect(double a1, double a2, double b1, double b2, double c1, double c2, double& x, double& y)
{
	double det = a1 * b2 - a2 * b1;
	x = (b1 * c2 - b2 * c1) / det;
	y = (a2 * c1 - a1 * c2) / det;
}

void CTriangGalerkinDlg::forceLines(vector<Dots> vecMagn, vector<Triangle> triangleABC, vector<vector<Dots>>& lineForce)
{
	vector<Dots> lineHelp;
	Dots helpers1;
	Dots helpers2;
	vector<Triangle> helperTriang;
	vector<Triangle> helperTriang1;
	Dots pointMin;
	double eps = 1.e-6;
	for (int i = 0; i < vecMagn.size(); i++)
	{
		int sh = 0;

		if (i < (vecMagn.size() - 1))
		{
			helpers1.x = (vecMagn[i + 1].x + vecMagn[i].x) / 2.;
			helpers1.y = (vecMagn[i + 1].y + vecMagn[i].y) / 2.;
		}
		else
		{
			helpers1.x = (vecMagn[i].x + vecMagn[0].x) / 2.;
			helpers1.y = (vecMagn[i].y + vecMagn[0].y) / 2.;
		}

		//helpers1.x = vecMagn[i].x;
		//helpers1.y = vecMagn[i].y;
		lineHelp.push_back(helpers1);

		while (helpers1.x > oblX_1 && helpers1.x < oblX_3 && helpers1.y > oblY_1 && helpers1.y < oblY_3 && sh < 1000)
		{
			helperTriang.clear();
			helperTriang1.clear();
			//ближайший узел к пробному заряду
			double rasst_min = (PointsSverch[0].x - helpers1.x) * (PointsSverch[0].x - helpers1.x) +
				(PointsSverch[0].y - helpers1.y) * (PointsSverch[0].y - helpers1.y);
			pointMin = PointsSverch[0];
			for (int k = 1; k < PointsSverch.size(); k++)
			{
				double p1 = (PointsSverch[k].x - helpers1.x) * (PointsSverch[k].x - helpers1.x);
				double p2 = (PointsSverch[k].y - helpers1.y) * (PointsSverch[k].y - helpers1.y);
				double rasst = p1 + p2;

				if (rasst < rasst_min)
				{
					rasst_min = rasst;
					pointMin = PointsSverch[k];
				}
			}

			//треугольники с найденным выше узлом
			for (int k = 0; k < triangleABC.size(); k++)
			{
				if ((abs(pointMin.x - triangleABC[k].A.x) < eps && abs(pointMin.y - triangleABC[k].A.y) < eps) ||
					(abs(pointMin.x - triangleABC[k].B.x) < eps && abs(pointMin.y - triangleABC[k].B.y) < eps) ||
					(abs(pointMin.x - triangleABC[k].C.x) < eps && abs(pointMin.y - triangleABC[k].C.y) < eps))
				{
					helperTriang.push_back(triangleABC[k]);
				}
			}

			for (int k = 0; k < helperTriang.size(); k++)
			{
				//площадь треугольника АВС
				double triangS0 = GetS(helperTriang[k].A.x, helperTriang[k].A.y, helperTriang[k].B.x, helperTriang[k].B.y, helperTriang[k].C.x, helperTriang[k].C.y);

				//площадь треугольника Р1ВС
				double triangS11 = GetS(helpers1.x, helpers1.y, helperTriang[k].B.x, helperTriang[k].B.y, helperTriang[k].C.x, helperTriang[k].C.y);
				//площадь треугольника АВР1
				double triangS12 = GetS(helperTriang[k].A.x, helperTriang[k].A.y, helpers1.x, helpers1.y, helperTriang[k].C.x, helperTriang[k].C.y);
				//площадь треугольника АР1С
				double triangS13 = GetS(helperTriang[k].A.x, helperTriang[k].A.y, helperTriang[k].B.x, helperTriang[k].B.y, helpers1.x, helpers1.y);
				//сумма площадей маленьких треугольников
				double sum1 = triangS11 + triangS12 + triangS13;

				if (abs(triangS0 - sum1) <= eps)
				{
					helperTriang1.push_back(helperTriang[k]);
				}
			}

			Triangle workTriang = helperTriang1.back();

			//нашли коэффы для плоскости(треугольников)
			double A_plosk = workTriang.A.y * (workTriang.B.potenc - workTriang.C.potenc) + workTriang.B.y * (workTriang.C.potenc - workTriang.A.potenc) +
				workTriang.C.y * (workTriang.A.potenc - workTriang.B.potenc);
			double B_plosk = workTriang.A.potenc * (workTriang.B.x - workTriang.C.x) + workTriang.B.potenc * (workTriang.C.x - workTriang.A.x) +
				workTriang.C.potenc * (workTriang.A.x - workTriang.B.x);

			Dots next = helpers1;

			//вычислили координаты смещенной точки
			helpers2.x = helpers1.x - A_plosk;
			helpers2.y = helpers1.y - B_plosk;
			//lineHelp.push_back(helpers2);

			//вычислили коэффы для прямой из пробного заряда и его смещения
			double A_pryam = helpers1.y - helpers2.y;
			double B_pryam = helpers2.x - helpers1.x;
			double С_pryam = -helpers2.x * helpers1.y + helpers1.x * helpers2.y;

			//вычислили коэффы для прямой из вершин треугольника А и В
			double A_treugAB = workTriang.A.y - workTriang.B.y;
			double B_treugAB = workTriang.B.x - workTriang.A.x;
			double С_treugAB = -workTriang.B.x * workTriang.A.y + workTriang.A.x * workTriang.B.y;

			//вычислили коэффы для прямой из вершин треугольника В и С
			double A_treugBC = workTriang.B.y - workTriang.C.y;
			double B_treugBC = workTriang.C.x - workTriang.B.x;
			double С_treugBC = -workTriang.C.x * workTriang.B.y + workTriang.B.x * workTriang.C.y;

			//вычислили коэффы для прямой из вершин треугольника С и А
			double A_treugCA = workTriang.C.y - workTriang.A.y;
			double B_treugCA = workTriang.A.x - workTriang.C.x;
			double С_treugCA = -workTriang.A.x * workTriang.C.y + workTriang.C.x * workTriang.A.y;

			//нашли координаты пересечения прямой и стороной треугольника АВ
			Dots pointCrossAB;
			intersect(A_pryam, A_treugAB, B_pryam, B_treugAB, С_pryam, С_treugAB, pointCrossAB.x, pointCrossAB.y);

			//нашли координаты пересечения прямой и стороной треугольника ВС
			Dots pointCrossBC;
			intersect(A_pryam, A_treugBC, B_pryam, B_treugBC, С_pryam, С_treugBC, pointCrossBC.x, pointCrossBC.y);

			//нашли координаты пересечения прямой и стороной треугольника СА
			Dots pointCrossCA;
			intersect(A_pryam, A_treugCA, B_pryam, B_treugCA, С_pryam, С_treugCA, pointCrossCA.x, pointCrossCA.y);

			//площадь треугольника АВС
			double triangS0 = GetS(workTriang.A.x, workTriang.A.y, workTriang.B.x, workTriang.B.y, workTriang.C.x, workTriang.C.y);

			//площадь треугольника Р1ВС
			double triangS11 = GetS(pointCrossAB.x, pointCrossAB.y, workTriang.B.x, workTriang.B.y, workTriang.C.x, workTriang.C.y);
			//площадь треугольника АВР1
			double triangS12 = GetS(workTriang.A.x, workTriang.A.y, pointCrossAB.x, pointCrossAB.y, workTriang.C.x, workTriang.C.y);
			//площадь треугольника АР1С
			double triangS13 = GetS(workTriang.A.x, workTriang.A.y, workTriang.B.x, workTriang.B.y, pointCrossAB.x, pointCrossAB.y);
			//сумма площадей маленьких треугольников
			double sumAB = triangS11 + triangS12 + triangS13;

			//площадь треугольника Р2ВС
			double triangS21 = GetS(pointCrossBC.x, pointCrossBC.y, workTriang.B.x, workTriang.B.y, workTriang.C.x, workTriang.C.y);
			//площадь треугольника АВР2
			double triangS22 = GetS(workTriang.A.x, workTriang.A.y, pointCrossBC.x, pointCrossBC.y, workTriang.C.x, workTriang.C.y);
			//площадь треугольника АР2С
			double triangS23 = GetS(workTriang.A.x, workTriang.A.y, workTriang.B.x, workTriang.B.y, pointCrossBC.x, pointCrossBC.y);
			//сумма площадей маленьких треугольников
			double sumBC = triangS21 + triangS22 + triangS23;

			//площадь треугольника Р3ВС
			double triangS31 = GetS(pointCrossCA.x, pointCrossCA.y, workTriang.B.x, workTriang.B.y, workTriang.C.x, workTriang.C.y);
			//площадь треугольника АВР3
			double triangS32 = GetS(workTriang.A.x, workTriang.A.y, pointCrossCA.x, pointCrossCA.y, workTriang.C.x, workTriang.C.y);
			//площадь треугольника АР2С
			double triangS33 = GetS(workTriang.A.x, workTriang.A.y, workTriang.B.x, workTriang.B.y, pointCrossCA.x, pointCrossCA.y);
			//сумма площадей маленьких треугольников
			double sumCA = triangS31 + triangS32 + triangS33;

			/*helpers1 = pointCrossBC;*/

			if ((abs(triangS0 - sumAB) < eps) && (abs(triangS0 - sumBC) < eps) && (abs(triangS0 - sumCA) < eps))
			{
				double helpAB = (helpers1.x - pointCrossAB.x) * (helpers1.x - pointCrossAB.x) + (helpers1.y - pointCrossAB.y) * (helpers1.y - pointCrossAB.y);
				double helpBC = (helpers1.x - pointCrossBC.x) * (helpers1.x - pointCrossBC.x) + (helpers1.y - pointCrossBC.y) * (helpers1.y - pointCrossBC.y);
				double helpCA = (helpers1.x - pointCrossCA.x) * (helpers1.x - pointCrossCA.x) + (helpers1.y - pointCrossCA.y) * (helpers1.y - pointCrossCA.y);

				if (helpAB >= helpBC && helpAB >= helpCA)
				{
					helpers1 = pointCrossAB;
				}

				if (helpBC >= helpCA && helpBC >= helpAB)
				{
					helpers1 = pointCrossBC;
				}

				if (helpCA >= helpAB && helpCA >= helpBC)
				{
					helpers1 = pointCrossCA;
				}
			}
			else if ((abs(triangS0 - sumAB) < eps) && (abs(triangS0 - sumBC) > eps) && (abs(triangS0 - sumCA) < eps))
			{
				double helpAB = (helpers1.x - pointCrossAB.x) * (helpers1.x - pointCrossAB.x) + (helpers1.y - pointCrossAB.y) * (helpers1.y - pointCrossAB.y);
				double helpCA = (helpers1.x - pointCrossCA.x) * (helpers1.x - pointCrossCA.x) + (helpers1.y - pointCrossCA.y) * (helpers1.y - pointCrossCA.y);

				if (helpAB >= helpCA)
				{
					helpers1 = pointCrossAB;
				}
				else
				{
					helpers1 = pointCrossCA;
				}
			}
			else if ((abs(triangS0 - sumAB) < eps) && (abs(triangS0 - sumBC) < eps) && (abs(triangS0 - sumCA) > eps))
			{
				double helpAB = (helpers1.x - pointCrossAB.x) * (helpers1.x - pointCrossAB.x) + (helpers1.y - pointCrossAB.y) * (helpers1.y - pointCrossAB.y);
				double helpBC = (helpers1.x - pointCrossBC.x) * (helpers1.x - pointCrossBC.x) + (helpers1.y - pointCrossBC.y) * (helpers1.y - pointCrossBC.y);

				if (helpAB >= helpBC)
				{
					helpers1 = pointCrossAB;
				}
				else
				{
					helpers1 = pointCrossBC;
				}
			}
			else if ((abs(triangS0 - sumAB) > eps) && (abs(triangS0 - sumBC) < eps) && (abs(triangS0 - sumCA) < eps))
			{
				double helpBC = (helpers1.x - pointCrossBC.x) * (helpers1.x - pointCrossBC.x) + (helpers1.y - pointCrossBC.y) * (helpers1.y - pointCrossBC.y);
				double helpCA = (helpers1.x - pointCrossCA.x) * (helpers1.x - pointCrossCA.x) + (helpers1.y - pointCrossCA.y) * (helpers1.y - pointCrossCA.y);

				if (helpCA >= helpBC)
				{
					helpers1 = pointCrossCA;
				}
				else
				{
					helpers1 = pointCrossBC;
				}
			}

			// Находим угол наклона прямой относительно X
			double side_y = helpers1.y - next.y;
			double side_x = helpers1.x - next.x;

			double angle_rad = atan2(side_y, side_x);

			// Величина удлинения
			double r = 0.001;

			helpers1.x += r * cos(angle_rad);
			helpers1.y += r * sin(angle_rad);

			lineHelp.push_back(helpers1);
			sh++;
		}

		lineForce.push_back(lineHelp);
		lineHelp.clear();
		helperTriang.clear();
		helperTriang1.clear();
	}
}

void CTriangGalerkinDlg::equipotencLines(vector<Triangle> triangleABC, vector<vector<Dots>>& lineEqui)
{
	// Построение эквипотенциальных линий (по аналогии с изотермами)
	double pc_min, pc_max;

	pc_min = triangleABC[0].A.potenc;
	pc_max = triangleABC[0].A.potenc;

	for (int i = 0; i < triangleABC.size(); i++)
	{
		// max
		if (pc_max < triangleABC[i].A.potenc
			&& !triangleABC[i].A.north1 && !triangleABC[i].A.south1
			&& !triangleABC[i].A.north2 && !triangleABC[i].A.south2
			&& !triangleABC[i].A.gran)
		{
			pc_max = triangleABC[i].A.potenc;
		}

		if (pc_max < triangleABC[i].B.potenc
			&& !triangleABC[i].B.north1 && !triangleABC[i].B.south1
			&& !triangleABC[i].B.north2 && !triangleABC[i].B.south2
			&& !triangleABC[i].B.gran)
		{
			pc_max = triangleABC[i].B.potenc;
		}

		if (pc_max < triangleABC[i].C.potenc
			&& !triangleABC[i].C.north1 && !triangleABC[i].C.south1
			&& !triangleABC[i].C.north2 && !triangleABC[i].C.south2
			&& !triangleABC[i].C.gran)
		{
			pc_max = triangleABC[i].C.potenc;
		}

		// min
		if (pc_min > triangleABC[i].A.potenc
			&& !triangleABC[i].A.north1 && !triangleABC[i].A.south1
			&& !triangleABC[i].A.north2 && !triangleABC[i].A.south2
			&& !triangleABC[i].A.gran)
		{
			pc_min = triangleABC[i].A.potenc;
		}

		if (pc_min > triangleABC[i].B.potenc
			&& !triangleABC[i].B.north1 && !triangleABC[i].B.south1
			&& !triangleABC[i].B.north2 && !triangleABC[i].B.south2
			&& !triangleABC[i].B.gran)
		{
			pc_min = triangleABC[i].B.potenc;
		}

		if (pc_min > triangleABC[i].C.potenc
			&& !triangleABC[i].C.north1 && !triangleABC[i].C.south1
			&& !triangleABC[i].C.north2 && !triangleABC[i].C.south2
			&& !triangleABC[i].C.gran)
		{
			pc_min = triangleABC[i].C.potenc;
		}
	}


	for (int i = 0; i < triangleABC.size(); i++)
	{
		Dots p1 = triangleABC[i].A;	// min
		Dots p2 = triangleABC[i].B;
		Dots p3 = triangleABC[i].C;	// max

		// Шаг 2.
		if (p1.potenc > p2.potenc)
		{
			swap(p1, p2);
		}

		if (p1.potenc > p3.potenc)
		{
			swap(p1, p3);
		}

		if (p2.potenc > p3.potenc)
		{
			swap(p2, p3);
		}

		double step = (abs(pc_max) + abs(pc_min)) / 10.;
		for (double j = pc_min + step; j < pc_max; j += step)
		{
			vector<Dots> pair;
			if (j > p1.potenc && j < p3.potenc)
			{
				Dots dot1;
				dot1.x = p3.x - ((p3.potenc - j) * (p3.x - p1.x)) / (p3.potenc - p1.potenc);
				dot1.y = p3.y - ((p3.potenc - j) * (p3.y - p1.y)) / (p3.potenc - p1.potenc);

				pair.push_back(dot1);

				if (j > p2.potenc)
				{
					Dots dot2;
					dot2.x = p3.x - ((p3.potenc - j) * (p3.x - p2.x)) / (p3.potenc - p2.potenc);
					dot2.y = p3.y - ((p3.potenc - j) * (p3.y - p2.y)) / (p3.potenc - p2.potenc);

					pair.push_back(dot2);
				}

				else if (j < p2.potenc)
				{
					Dots dot2;
					dot2.x = p2.x - ((p2.potenc - j) * (p2.x - p1.x)) / (p2.potenc - p1.potenc);
					dot2.y = p2.y - ((p2.potenc - j) * (p2.y - p1.y)) / (p2.potenc - p1.potenc);

					pair.push_back(dot2);
				}
			}

			if (!pair.empty())
			{
				lineEqui.push_back(pair);
			}
		}
	}
}

void CTriangGalerkinDlg::OnBnClickedTriang()
{
	UpdateData(TRUE);
	PointsSverch.clear();	recTriang.clear();	 randPoints.clear();
	squareHelp1.clear();		squareHelp2.clear();
	square1.clear();		square2.clear();
	CalcDyrky();
	AnimationInformation();
}

void CTriangGalerkinDlg::OnBnClickedDelete()
{
	UpdateData(TRUE);

	Delete(recTriang);
	DrawTriang(PointsSverch, recTriang, PicDcGraph, PicGraph);
}

void CTriangGalerkinDlg::OnBnClickedGalerkin()
{
	UpdateData(TRUE);

	vector<Dots> vectorVnutr;
	for (int i = 0; i < PointsSverch.size(); i++)
	{
		if (PointsSverch[i].dr)
		{
			vectorVnutr.push_back(PointsSverch[i]);
		}
	}

	vector<Dots> vectorGran;
	for (int i = 0; i < PointsSverch.size(); i++)
	{
		if (PointsSverch[i].gran || PointsSverch[i].north1 || PointsSverch[i].south1 || PointsSverch[i].north2 || PointsSverch[i].south2)
		{
			vectorGran.push_back(PointsSverch[i]);
		}
	}

	vector<double> MatrA;
	matrixAij(vectorVnutr, MatrA);
	vector<double> vecB;
	vectorBj(vectorVnutr, vectorGran, vecB);

	vector<double> vecPhi(vectorVnutr.size());

	kazf(MatrA, vecB, vecPhi, vecPhi.size(), vecB.size());

	double eps = 1.e-4;
	for (int i = 0; i < vectorVnutr.size(); i++)
	{
		for (int k = 0; k < PointsSverch.size(); k++)
		{
			if (abs(PointsSverch[k].x - vectorVnutr[i].x) < eps && abs(PointsSverch[k].y - vectorVnutr[i].y) < eps)
			{
				PointsSverch[k].potenc = vecPhi[i];
			}
		}
	}

	for (int i = 0; i < vectorVnutr.size(); i++)
	{
		for (int k = 0; k < recTriang.size(); k++)
		{

			if ((abs(vectorVnutr[i].x - recTriang[k].A.x) < eps && abs(vectorVnutr[i].y - recTriang[k].A.y) < eps))
			{
				recTriang[k].A.potenc = vecPhi[i];
			}

			if ((abs(vectorVnutr[i].x - recTriang[k].B.x) < eps && abs(vectorVnutr[i].y - recTriang[k].B.y) < eps))
			{
				recTriang[k].B.potenc = vecPhi[i];
			}

			if ((abs(vectorVnutr[i].x - recTriang[k].C.x) < eps && abs(vectorVnutr[i].y - recTriang[k].C.y) < eps))
			{
				recTriang[k].C.potenc = vecPhi[i];
			}
		}
	}

	MessageBox(L"Галеркин успешно завершил свою работу!", L"Information", MB_OK || MB_ICONERROR);

	if (potenc_draw.GetCheck() == BST_CHECKED)
	{
		DrawTriang(PointsSverch, recTriang, PicDcGraph, PicGraph);
	}
	/*ofstream f("kazf.txt");
	for (int i = 0; i < vecPhi.size(); i++)
	{
		f << endl;
		f << setprecision(3) << vecPhi[i];
	}
	f.close();*/
}

void CTriangGalerkinDlg::OnBnClickedForce()
{
	UpdateData(TRUE);

	vector<Dots> vecMagn1;
	for (int i = 0; i < PointsSverch.size(); i++)
	{
		if (PointsSverch[i].north1 || PointsSverch[i].south1)
		{
			vecMagn1.push_back(PointsSverch[i]);
		}
	}

	vector<Dots> vecMagn2;
	for (int i = 0; i < PointsSverch.size(); i++)
	{
		if (PointsSverch[i].north2 || PointsSverch[i].south2)
		{
			vecMagn2.push_back(PointsSverch[i]);
		}
	}

	vector<vector<Dots>> forceLine;
	forceLines(vecMagn1, recTriang, forceLine);
	vecMagn1.clear();
	forceLines(vecMagn2, recTriang, forceLine);	
	vecMagn2.clear();

	/*ofstream f("lineForce.txt");
	for (int i = 0; i < forceLine.size(); i++)
	{
		for (int j = 0; j < forceLine[i].size(); j++)
		{
			f << "x = " << setprecision(3) << forceLine[i][j].x << "\t";
			f << "y = " << setprecision(3) << forceLine[i][j].y << "\t";
			f << "z = " << setprecision(3) << forceLine[i][j].z << "\t";
			f << "potenc = " << setprecision(3) << forceLine[i][j].potenc << "\t";
			f << endl;

			f << endl << endl;
		}
	}

	f.close();*/

	DrawForce(forceLine, PointsSverch, recTriang, PicDcGraph, PicGraph);
}

void CTriangGalerkinDlg::OnBnClickedEquiline()
{
	UpdateData(TRUE);

	vector<vector<Dots>> lineEquiPotenc;
	equipotencLines(recTriang, lineEquiPotenc);

	/*ofstream f("lineEqui.txt");
	for (int i = 0; i < recTriang.size(); i++)
	{
		f << "xA = " << setprecision(3) << recTriang[i].A.x << "\t";
		f << "yA = " << setprecision(3) << recTriang[i].A.y << "\t";
		f << "zA = " << setprecision(3) << recTriang[i].A.z << "\t";
		f << "potencA = " << setprecision(3) << recTriang[i].A.potenc << "\t";
		f << endl;

		f << "xB = " << setprecision(3) << recTriang[i].B.x << "\t";
		f << "yB = " << setprecision(3) << recTriang[i].B.y << "\t";
		f << "zB = " << setprecision(3) << recTriang[i].B.z << "\t";
		f << "potencB = " << setprecision(3) << recTriang[i].B.potenc << "\t";
		f << endl;

		f << "xC = " << setprecision(3) << recTriang[i].C.x << "\t";
		f << "yC = " << setprecision(3) << recTriang[i].C.y << "\t";
		f << "zC = " << setprecision(3) << recTriang[i].C.z << "\t";
		f << "potencC = " << setprecision(3) << recTriang[i].C.potenc << "\t";

		f << endl << endl;
	}

	f.close();*/


	DrawEqui(lineEquiPotenc, PointsSverch, recTriang, PicDcGraph, PicGraph);
}

void CTriangGalerkinDlg::OnBnClickedCancel()
{
	// TODO: добавьте свой код обработчика уведомлений
	CDialogEx::OnCancel();
}