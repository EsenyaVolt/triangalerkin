﻿
// TriangGalerkin.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CTriangGalerkinApp:
// Сведения о реализации этого класса: TriangGalerkin.cpp
//

class CTriangGalerkinApp : public CWinApp
{
public:
	CTriangGalerkinApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CTriangGalerkinApp theApp;
