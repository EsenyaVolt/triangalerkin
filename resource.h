﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется TriangGalerkin.rc
//
#define IDD_TRIANGGALERKIN_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDC_EDIT1                       1000
#define IDC_EDIT2                       1001
#define IDC_EDIT3                       1002
#define IDC_EDIT4                       1003
#define IDC_EDIT5                       1004
#define IDC_EDIT6                       1005
#define IDC_EDIT7                       1006
#define IDC_EDIT8                       1007
#define IDC_EDIT9                       1008
#define IDC_EDIT10                      1009
#define IDC_EDIT11                      1010
#define IDC_EDIT12                      1011
#define IDC_EDIT13                      1012
#define IDC_EDIT14                      1013
#define IDC_EDIT15                      1014
#define IDC_EDIT16                      1015
#define IDC_EDIT17                      1016
#define IDC_EDIT18                      1017
#define IDC_EDIT19                      1018
#define IDC_EDIT20                      1019
#define IDC_EDIT21                      1020
#define IDC_EDIT22                      1021
#define IDC_EDIT23                      1022
#define IDC_EDIT24                      1023
#define IDC_EDIT25                      1024
#define IDC_EDIT26                      1025
#define IDC_TRIANG                      1026
#define IDC_DELETE                      1027
#define IDC_EDIT27                      1028
#define IDC_GRAPH                       1029
#define IDC_EDIT29                      1032
#define IDC_EDIT30                      1033
#define IDC_GALERKIN                    1034
#define IDC_BUTTON1                     1035
#define IDC_EQUILINE                    1036
#define IDC_POTENC                      1038

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1040
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
